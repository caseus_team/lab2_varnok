package varnok.model;

import varnok.math.Operations;
import varnok.transformation.ParallelProjection;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ColorPolygon extends Polygon {

    private Color color;
    protected List<Vertex> vertexList = new ArrayList();
    public int[] zpoints;
    private int xMin;
    private int xMax;
    private int yMin;
    private int yMax;
    private char index;
    private PlaneEquation planeEquation;

    public ColorPolygon(Color color, char index) {
        super();
        this.color = color;
        this.index = index;
    }

    public ColorPolygon(Color color, char index, Vertex... vertexes) {
        this.planeEquation = Operations.getPlaneEquation(vertexes);
        System.out.println("color = [" + color + "], index = [" + index + "], vertexes = \n[" +
                Arrays.stream(vertexes).map(Vertex::toString).collect(Collectors.joining("\n")) + "]");
        System.out.println("Plane equation: " + planeEquation.toString());
        zpoints = new int[vertexes.length];
        for (int i = 0; i < vertexes.length; i++) {
            zpoints[i] = (int) vertexes[i].getZ();
        }
        Arrays.stream(vertexes).forEach(vertex -> {
            vertex.doTransform(new ParallelProjection(Plane.XY));
        });
        Arrays.stream(vertexes).forEach(this::addPoint);
        this.color = color;
        xMin = Arrays.stream(xpoints).limit(npoints).min().getAsInt();
        xMax = Arrays.stream(xpoints).limit(npoints).max().getAsInt();
        yMin = Arrays.stream(ypoints).limit(npoints).min().getAsInt();
        yMax = Arrays.stream(ypoints).limit(npoints).max().getAsInt();
        this.index = index;
        System.out.println("---------------");
        for (int i = 0; i < npoints; i++) {
            System.out.println(new Vertex(xpoints[i], ypoints[i], zpoints[i]));
        }
    }

    public void addPoint(Vertex vertex) {
        addPoint((int)vertex.getX(), (int)vertex.getY());
    }

    public List<Vertex> getVertexList() {
        return vertexList;
    }

    public int getxMin() {
        return xMin;
    }

    public int getxMax() {
        return xMax;
    }

    public int getyMin() {
        return yMin;
    }

    public int getyMax() {
        return yMax;
    }

    public Color getColor() {
        return color;
    }

    public char getIndex() {
        return index;
    }

    @Override
    public String toString() {
        return "polygon # " + index;
    }

    public PlaneEquation getPlaneEquation() {
        return planeEquation;
    }
}
