package varnok.model;

import java.util.Arrays;

public class Window {

    private int x;
    private int y;
    private int w;

    public Window(int x, int y, int w) {
        this.x = x;
        this.y = y;
        this.w = w;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getW() {
        return w;
    }

    @Override
    public String toString() {
        return Arrays.toString(new int[]{x, y, w});
    }
}
