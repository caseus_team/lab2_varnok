package varnok.model;

import java.util.Optional;

public class PlaneEquation {

    private double a, b, c, d;

    public PlaneEquation(double a, double b, double c, double d) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }

    public Optional<Double> countZ(double x, double y) {
        if (Math.abs(c) < 1e-6)
            return Optional.empty();
        return Optional.of(-(a * x + b * y + d) / c);
    }

    @Override
    public String toString() {
        return a + " " + b + " " + c + " " + d;
    }
}
