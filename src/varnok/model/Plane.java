package varnok.model;

public enum Plane {

    YZ, XZ, XY, CAB;

}
