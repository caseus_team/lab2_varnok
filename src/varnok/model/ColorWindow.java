package varnok.model;

import java.awt.*;

public class ColorWindow extends ColorPolygon {

    private int xLeft;
    private int xRight;
    private int yBottom;
    private int yTop;

    public ColorWindow(Color color, char index, int x, int y, int w) {
        super(color, index);
        xLeft = x;
        xRight = x + w;
        yBottom = y;
        yTop = y + w;
        vertexList.add(new Vertex(xLeft, yBottom, 0));
        vertexList.add(new Vertex(xLeft, yTop, 0));
        vertexList.add(new Vertex(xRight, yTop, 0));
        vertexList.add(new Vertex(xRight, yBottom, 0));
        vertexList.forEach(this::addPoint);
    }

    public ColorWindow(Color color, Window window) {
        this(color, '.', window.getX(), window.getY(), window.getW());
    }

    public int getxLeft() {
        return xLeft;
    }

    public int getxRight() {
        return xRight;
    }

    public int getyBottom() {
        return yBottom;
    }

    public int getyTop() {
        return yTop;
    }
}
