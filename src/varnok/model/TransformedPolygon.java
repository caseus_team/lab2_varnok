package varnok.model;

import varnok.transformation.Scale;
import varnok.rendering.Frame;

public class TransformedPolygon extends ColorPolygon {

    public TransformedPolygon(ColorPolygon colorPolygon) {
        super(colorPolygon.getColor(), colorPolygon.getIndex());
        for (int i = 0; i < colorPolygon.npoints; i++) {
            try {
                Vertex vertex = new Vertex(colorPolygon.xpoints[i], colorPolygon.ypoints[i], 0);
                vertex.doTransform(new Scale(Frame.SCALE, Axis.values()));
//                vertex.getCoordinates()[0] = convertX(vertex.getX(), Frame.WIDTH);
//                vertex.getCoordinates()[1] = convertY(vertex.getY(), Frame.HEIGHT);
                addPoint(vertex);
            } catch (Exception e) {
                System.out.println("colorPolygon = [" + colorPolygon + "]");
            }
        }
    }

    private double convertX(double x, double width) {
        return x + width / 2;
    }

    private double convertY(double y, double height) {
        return -y + height / 2;
    }
}
