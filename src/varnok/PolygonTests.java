package varnok;

import varnok.model.ColorPolygon;
import varnok.model.Window;

import java.awt.*;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.lang.Math.signum;

public class PolygonTests {

    private int xLeft;
    private int xRight;
    private int yBottom;
    private int yTop;
    private int xMin;
    private int xMax;
    private int yMin;
    private int yMax;
    private ColorPolygon colorPolygon;
    private Window window;

    public PolygonTests(Window window) {
        this.window = window;
        xLeft = window.getX();
        xRight = window.getX() + window.getW();
        yBottom = window.getY();
        yTop = window.getY() + window.getW();
    }

    public boolean polygonIsOutOfWindow() {
        return xMin > xRight - 1 || xMax < xLeft || yMin > yTop - 1 || yMax < yBottom;
    }

    public boolean polygonIsInWindow() {
        return xMin >= xLeft && xMax <= xRight && yMin >= yBottom && yMax <= yTop;
    }

    public boolean polygonIsAroundWindow() {
        double x = window.getW() / 2.0 + window.getX();
        double y = window.getW() / 2.0 + window.getY();
        for (int i = 0; i < colorPolygon.npoints - 1; i++) {
            Point a = new Point(colorPolygon.xpoints[i], colorPolygon.ypoints[i]);
            Point b = new Point(colorPolygon.xpoints[i + 1], colorPolygon.ypoints[i + 1]);
            if (countMultiply(x, y, a, b) < 0)
                return false;
        }
        Point a = new Point(colorPolygon.xpoints[colorPolygon.npoints - 1], colorPolygon.ypoints[colorPolygon.npoints - 1]);
        Point b = new Point(colorPolygon.xpoints[0], colorPolygon.ypoints[0]);
        if (countMultiply(x, y, a, b) < 0)
            return false;
        return true;
    }

    private double countMultiply(double x, double y, Point a, Point b) {
        return signum((x - a.x) * (b.y - a.y) - (y - a.y) * (b.x - a.x));
    }

    public boolean polygonInterceptWindow() {
        double m = 0, b = 0;
        for (int i = 0; i < colorPolygon.npoints - 1; i++){
            int x1 = colorPolygon.xpoints[i];
            int x2 = colorPolygon.xpoints[i + 1];
            int y1 = colorPolygon.ypoints[i];
            int y2 = colorPolygon.ypoints[i + 1];
            if (x2 != x1) {
                m = (y2 - y1) * 1.0 / (x2 - x1);
                b = y2 - m * x2;
                break;
            }
        }
        double mm = m, bb = b;
        List<Double> collect = Stream.of(
                new Point(xLeft, yBottom),
                new Point(xLeft, yTop),
                new Point(xRight, yTop),
                new Point(xRight, yBottom)
        ).map(point -> testFunction(mm, bb, point.x, point.y)).collect(Collectors.toList());
        Double min = collect.stream().min(Double::compareTo).get();
        Double max = collect.stream().max(Double::compareTo).get();
        return (Math.signum(min) != Math.signum(max));
    }

    public double testFunction(double m, double b, int x, int y) {
        return y - m * x - b;
    }

    public void setColorPolygon(ColorPolygon colorPolygon) {
        this.colorPolygon = colorPolygon;
        xMin = colorPolygon.getxMin();
        xMax = colorPolygon.getxMax();
        yMax = colorPolygon.getyMax();
        yMin = colorPolygon.getyMin();
    }
}
