package varnok.rendering;

import varnok.model.ColorPolygon;
import varnok.model.Vertex;

import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Rendering {

    public static List<ColorPolygon> polygons = new ArrayList<>();
    private static final Color[] colors = new Color[]{Color.GREEN, Color.RED, Color.ORANGE, Color.YELLOW, Color.CYAN, Color.BLUE};

    static {
        Scanner scanner = null;
        try {
            scanner = new Scanner(new FileReader(new File("input.txt")));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        int polygonsNumber = scanner.nextInt();
        for (int i = 0; i < polygonsNumber; i++) {
            int vertexNumber = scanner.nextInt();
            Vertex[] vertices = new Vertex[vertexNumber];
            for (int j = 0; j < vertexNumber; j++) {
                vertices[j] = new Vertex(scanner.nextInt(), scanner.nextInt(), scanner.nextInt());
            }
            polygons.add(new ColorPolygon(
                    colors[i], String.valueOf(i + 1).charAt(0), vertices
            ));
        }
    }

}
