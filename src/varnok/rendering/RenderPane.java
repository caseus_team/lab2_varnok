package varnok.rendering;

import varnok.model.*;
import varnok.model.Window;
import varnok.transformation.Transformation;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class RenderPane {
    private Transformation projection;
    private Plane currentPlane;
    private List<ColorPolygon> polygonList = Rendering.polygons;
    private List<ColorWindow> windows = new ArrayList<>();
    private ColorWindow window;
    private Graphics2D g2;

    public void setWindow(Window window) {
        this.window = new ColorWindow(Color.BLUE, window);
    }

    public void addDrawingWindow(Window window, Color color, char index) {
        windows.add(new ColorWindow(color, index, window.getX(), window.getY(), window.getW()));
    }

    private void drawWindow() {
        if (window == null)
            return;
        TransformedPolygon transformedPolygon = new TransformedPolygon(window);
        g2.setColor(transformedPolygon.getColor());
        g2.draw(transformedPolygon);
    }

    private void drawPolygon(ColorPolygon polygon) {
        TransformedPolygon transformedPolygon = new TransformedPolygon(polygon);
        g2.setColor(Color.BLACK);
        g2.draw(transformedPolygon);
        g2.setColor(transformedPolygon.getColor());
        g2.fillPolygon(transformedPolygon);
    }

    private void drawColorWindow(ColorWindow colorWindow) {
        TransformedPolygon transformedPolygon = new TransformedPolygon(colorWindow);
        g2.setColor(transformedPolygon.getColor());
        g2.draw(transformedPolygon);
        g2.fillPolygon(transformedPolygon);
    }

    public void paintComponent(Graphics g) {
        g2 = (Graphics2D) g;
        drawShapes();
        drawWindow();
        drawWindows();
    }

    private void drawWindows() {
        windows.forEach(this::drawColorWindow);
    }

    private void drawShapes() {
        polygonList.forEach(this::drawPolygon);
    }
}
