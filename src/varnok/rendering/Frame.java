package varnok.rendering;

import javax.swing.*;
import java.awt.*;

public class Frame extends JFrame {

    public static int HEIGHT = 128;
    public static int WIDTH = 128;
    public static int SCALE = 4;
    public static Color BACKGROUND_COLOR = Color.WHITE;
    private RenderPane renderPane;
    private JPanel scene;

    public Frame()  {
        super();
        Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());
        this.setBackground(BACKGROUND_COLOR);

        renderPane = new RenderPane();

        scene = new JPanel() {
            @Override
            public void paint(Graphics g) {
                super.paint(g);
                renderPane.paintComponent(g);
            }
        };
        scene.setBackground(Color.LIGHT_GRAY);
        scene.setBounds(0, 0, WIDTH * SCALE, HEIGHT * SCALE);
        scene.setBorder(BorderFactory.createLineBorder(Color.GRAY));
        this.add(scene);

        setSize(WIDTH * SCALE + 50, HEIGHT * SCALE + 50);
        setVisible(true);

        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    public RenderPane getRenderPane() {
        return renderPane;
    }

    public JPanel getScene() {
        return scene;
    }
}
