package varnok.math;

import varnok.model.PlaneEquation;
import varnok.model.Vertex;

public class Operations {

    public static double[][] multiply(double[][] a, double[][] b) {
        double[][] c = new double[a.length][b[0].length];
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < b[0].length; j++) {
                c[i][j] = 0;
                for (int k = 0; k < b.length; k++) {
                    c[i][j] += a[i][k] * b[k][j];
                }
            }
        }
        return c;
    }

    public static double[] multiply(double[][] matrix, double[] vector) {
        double[] result = new double[vector.length];
        for (int i = 0; i < matrix.length; i++) {
            result[i] = 0;
            for (int k = 0; k < vector.length; k++) {
                result[i] += matrix[i][k] * vector[k];
            }
        }
        return result;
    }

    public static double[] multiply(double[] vector, double[][] matrix) {
        double[] result = new double[vector.length];
        for (int j = 0; j < matrix[0].length; j++) {
            result[j] = 0;
            for (int r = 0; r < matrix.length; r++) {
                result[j] += vector[r] * matrix[r][j];
            }
        }
        return result;
    }

    public static PlaneEquation getPlaneEquation(Vertex[] vertexes) {
        double x1 = vertexes[0].getX();
        double y1 = vertexes[0].getY();
        double z1 = vertexes[0].getZ();
        double x2 = vertexes[1].getX();
        double y2 = vertexes[1].getY();
        double z2 = vertexes[1].getZ();
        double x3 = vertexes[2].getX();
        double y3 = vertexes[2].getY();
        double z3 = vertexes[2].getZ();
        double a1 = x2 - x1;
        double b1 = y2 - y1;
        double c1 = z2 - z1;
        double a2 = x3 - x1;
        double b2 = y3 - y1;
        double c2 = z3 - z1;
        double a = b1 * c2 - b2 * c1;
        double b = a2 * c1 - a1 * c2;
        double c = a1 * b2 - b1 * a2;
        double d = (- a * x1 - b * y1 - c * z1);
        return new PlaneEquation(a, b, c, d);
    }

}
