package varnok.math;

public interface MatrixGetter {

    double[][] getMatrix();

}
