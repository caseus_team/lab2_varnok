package varnok.math;

public interface MatrixGetterWithParameter {
    
    double[][] getMatrix(double value);
    
}
