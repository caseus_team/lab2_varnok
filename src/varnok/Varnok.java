package varnok;

import varnok.rendering.Frame;

public class Varnok {

    public static void main(String[] args) {
        Frame frame = new Frame();
        new Method(frame).process();
        frame.getScene().repaint();
    }

}
