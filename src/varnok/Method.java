package varnok;

import varnok.model.ColorPolygon;
import varnok.model.Window;
import varnok.rendering.Frame;
import varnok.rendering.RenderPane;
import varnok.rendering.Rendering;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

import static java.lang.Thread.sleep;

public class Method {

    private JPanel scene;
    private List<ColorPolygon> polygons;
    private RenderPane renderPane;
    private char[][] field = new char[Frame.WIDTH][Frame.HEIGHT];

    public Method(Frame frame) {
        this.scene = frame.getScene();
        this.renderPane = frame.getRenderPane();
        polygons = Rendering.polygons;
        for (int i = 0; i < Frame.WIDTH; i++) {
            for (int j = 0; j < Frame.HEIGHT; j++) {
                field[i][j] = '.';
            }
        }
    }

    public void process() {
        Stack<Window> stack = new Stack<>();
        stack.push(new Window(0, 0, Frame.WIDTH));
        while (!stack.empty()) {
            Window window = stack.pop();
            renderPane.setWindow(window);
            repaint();
            PolygonTests polygonTests = new PolygonTests(window);
            boolean hasNotExternal = false;
            for (ColorPolygon polygon : polygons) {
                polygonTests.setColorPolygon(polygon);
                if (!polygonTests.polygonIsOutOfWindow()) {
                    hasNotExternal = true;
                    break;
                }
            }
            if (hasNotExternal) {
                if (window.getW() > 1) {
                    int w = window.getW();
                    int x = window.getX();
                    int y = window.getY();
                    int add = w % 2;
                    w /= 2;
                    w += add;
                    stack.push(new Window(x + w, y + w, w));
                    stack.push(new Window(x, y + w, w));
                    stack.push(new Window(x + w, y, w));
                    stack.push(new Window(x, y, w));
                    continue;
                }
                else {
                    checkSmallWindow(window, polygons, polygonTests);
                    continue;
                }
            }
            colorField(window, '0');
            drawEmptyWindow(window);
        }
        for (char[] aField : field) {
            System.out.println(aField);
        }
    }

    private void colorField(Window window, char index) {
        for (int i = 0; i < window.getW(); i++) {
            for (int j = 0; j < window.getW(); j++) {
                field[window.getX() + i][window.getY() + j] = index;
            }
        }
    }

    private void checkSmallWindow(Window window, List<ColorPolygon> polygons, PolygonTests polygonTests) {
        List<ColorPolygon> covering = new ArrayList<>();
        for (ColorPolygon polygon : polygons) {
            polygonTests.setColorPolygon(polygon);
            if (polygonTests.polygonIsAroundWindow()) {
                covering.add(polygon);
            }
        }
        if (covering.isEmpty()) {
            drawEmptyWindow(window);
            return;
        }
        drawWindow(window, findColorPolygonOfMaxZ(covering, window));
    }

    private ColorPolygon findColorPolygonOfMaxZ(List<ColorPolygon> colorPolygons, Window window) {
        double x = window.getW() / 2.0 + window.getX();
        double y = window.getW() / 2.0 + window.getY();
        ColorPolygon polygon = colorPolygons.get(0);
        double maxZ = Double.MIN_VALUE;
        for (ColorPolygon colorPolygon : colorPolygons) {
            double z = colorPolygon.getPlaneEquation().countZ(x, y).orElse(
                    Arrays.stream(colorPolygon.zpoints).max().getAsInt() * 1.0
            );
            System.out.println("=======z: " + z);
            if (z - maxZ > 1e-6) {
                maxZ = z;
                polygon = colorPolygon;
            }
        }
        return polygon;
    }

    private void drawWindow(Window window, ColorPolygon colorPolygon) {
        renderPane.addDrawingWindow(window, colorPolygon.getColor(), colorPolygon.getIndex());
        colorField(window, colorPolygon.getIndex());
        repaint();
    }

    private void drawEmptyWindow(Window window) {
        renderPane.addDrawingWindow(window, Frame.BACKGROUND_COLOR, '0');
        colorField(window, '0');
        repaint();
    }

    private void repaint() {
        scene.paintImmediately(0, 0, Frame.WIDTH * Frame.SCALE, Frame.HEIGHT * Frame.SCALE);
        try {
            sleep(0);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
