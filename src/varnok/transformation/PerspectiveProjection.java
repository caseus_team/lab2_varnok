package varnok.transformation;

import varnok.math.Operations;
import varnok.model.Plane;

public class PerspectiveProjection extends Transformation {

    private static double D = 1000;

    @Override
    public double[] multiply(double[] vector) {
        return Operations.multiply(vector, matrix);
    }

    public PerspectiveProjection(Plane plane) {
        super(new double[][] {
                {1, 0, 0, 0},
                {0, 1, 0, 0},
                {0, 0, 1, 0},
                {0, 0, 0, 1}
        });
        matrix[plane.ordinal()][3] = - 1/D;
        matrix[plane.ordinal()][plane.ordinal()] = 0;
    }

}
