package varnok.transformation;

import varnok.model.Axis;

public class Scale extends Transformation {

    public Scale(double value, Axis... axises) {
        super(new double[][]{
                {1, 0, 0, 0},
                {0, 1, 0, 0},
                {0, 0, 1, 0},
                {0, 0, 0, 1}
        });
        for (Axis axis : axises) {
            int ordinal = axis.ordinal();
            matrix[ordinal][ordinal] = value;
        }
    }



}
