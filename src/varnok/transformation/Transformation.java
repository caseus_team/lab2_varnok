package varnok.transformation;

import varnok.math.Operations;

import java.util.Arrays;

public class Transformation {

    protected double[][] matrix;

    public Transformation(double[][] matrix) {
        this.matrix = matrix;
    }

    public double[] multiply(double[] vector) {
        return Operations.multiply(vector, matrix);
    }

    public double[] doTransform(double[] vector) {
        return normalize(multiply(vector));
    }

    private double[] normalize(double[] vector) {
        return Arrays.stream(vector).map(operand -> operand / vector[vector.length - 1]).toArray();
    }

}
