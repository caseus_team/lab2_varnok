package varnok.transformation;

import varnok.model.Axis;

public class Moving extends Transformation {

    public Moving(Axis axis, double value) {
        super(new double[][]{
                {1, 0, 0, 0},
                {0, 1, 0, 0},
                {0, 0, 1, 0},
                {0, 0, 0, 1}
        });
        matrix[3][axis.ordinal()] = value;
    }
}
