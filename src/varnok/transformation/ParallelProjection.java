package varnok.transformation;

import varnok.model.Plane;
import varnok.math.MatrixGetter;

import java.util.EnumMap;
import java.util.Map;

public class ParallelProjection extends Transformation {

    private static double alpha = Math.cos(Math.PI / 4) / 2;
    private static double beta = Math.cos(Math.PI / 4) / 2;

    private static Map<Plane, MatrixGetter> projectionMatrix = new EnumMap<>(Plane.class);

    static {
        projectionMatrix.put(Plane.XY, ParallelProjection::getProjectionOnXY);
        projectionMatrix.put(Plane.YZ, ParallelProjection::getProjectionOnYZ);
        projectionMatrix.put(Plane.XZ, ParallelProjection::getProjectionOnXZ);
    }

    public ParallelProjection(Plane plane) {
        super(projectionMatrix.get(plane).getMatrix());

    }

    private static double[][] getProjectionOnXY() {
        return new double[][] {
                {1, 0, 0, 0},
                {0, 1, 0, 0},
                {alpha, beta, 0, 0},
                {0, 0, 0, 1}
        };
    }

    private static double[][] getProjectionOnXZ() {
        return new double[][] {
                {1, 0, 0, 0},
                {alpha, 0, beta, 0},
                {0, 0, 1, 0},
                {0, 0, 0, 1}
        };
    }

    private static double[][] getProjectionOnYZ() {
        return new double[][] {
                {0, alpha, beta, 0},
                {0, 1, 0, 0},
                {0, 0, 1, 0},
                {0, 0, 0, 1}
        };
    }
}
