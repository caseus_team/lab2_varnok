package varnok.transformation;

import varnok.math.MatrixGetter;
import varnok.math.Operations;
import varnok.model.Plane;

import java.util.EnumMap;
import java.util.Map;

public class ProjectionOnPlane extends Transformation {

    private static Map<Plane, MatrixGetter> projectionMatrix = new EnumMap<>(Plane.class);

    static {
        projectionMatrix.put(Plane.XY, ProjectionOnPlane::getProjectionOnXY);
        projectionMatrix.put(Plane.YZ, ProjectionOnPlane::getProjectionOnYZ);
        projectionMatrix.put(Plane.XZ, ProjectionOnPlane::getProjectionOnXZ);
    }

    @Override
    public double[] multiply(double[] vector) {
        return Operations.multiply(matrix, vector);
    }

    public ProjectionOnPlane(Plane plane) {
        super(projectionMatrix.get(plane).getMatrix());
    }

    private static double[][] getProjectionOnXY() {
        return new double[][] {
                {1, 0, 0, 0},
                {0, 1, 0, 0},
                {0, 0, 0, 0},
                {0, 0, 0, 1}
        };
    }

    private static double[][] getProjectionOnXZ() {
        return new double[][] {
                {1, 0, 0, 0},
                {0, 0, 0, 0},
                {0, 0, 1, 0},
                {0, 0, 0, 1}
        };
    }

    private static double[][] getProjectionOnYZ() {
        return new double[][] {
                {0, 0, 0, 0},
                {0, 1, 0, 0},
                {0, 0, 1, 0},
                {0, 0, 0, 1}
        };
    }
}
